﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using HeyTeacherCommon.CommunicationModels;
using HeyTeacherServer.Models;
using System.Security.Cryptography;
using System.Text;
using HeyTeacherCommon.CommunicationModels.CommunicationModels;

namespace HeyTeacherServer.Database
{
    public class Database
    {
        private static Database _database;
        
        public static Database Instance
        {
            get
            {
                if(_database == null)
                {
                    _database = new Database();
                }
                return _database;
            }
        }


        private SqlConnection sqlConnection;

        private Database()
        {
            sqlConnection = new SqlConnection();
            sqlConnection.ConnectionString = "Data Source=heyteacher.database.windows.net;Initial Catalog=HeyTeacherDB;UID=heyadmin;PWD=Heypassword1;Encrypt=yes; Max Pool Size=10";
        }

        public RequestUser GetUserFromLogin(string login)
        {
            sqlConnection.Open();
            string queryString = @"SELECT u.FirstName, u.LastName, u.CreateAt FROM Users u
                                   WHERE u.Login=@login";
            SqlCommand command = new SqlCommand(queryString, sqlConnection);
            command.Parameters.Add(new SqlParameter("@login", login));
            SqlDataReader reader = command.ExecuteReader();
            if(reader.HasRows==false)
            {
                sqlConnection.Close();
                return null;
            }
            reader.Read();
            RequestUser requestUser = new RequestUser(login, (string)reader[0], (string)reader[1], (DateTime)reader[2]);
            sqlConnection.Close();
            return requestUser;
        }

        public List<RequestPost> GetAllPostsForUser(int userid)
        {
            sqlConnection.Open();
            List<RequestPost> postsList = new List<RequestPost>();
            string queryString = @"SELECT p.PostID, p.Text, p.ModifiedAt, p.UserID,
                                (select Login from Users where UserID = p.UserID) as Login, 
                                p.GroupID, 
                                (select GroupName from Groups where GroupId = p.GroupId) as GroupName, 
                                p.Points,
                                (select TypeName from PostTypes where PostTypeID = p.PostTypeID) as TypeName, 
                                p.Topic,
                                (select Color from PostTypes where PostTypeID = p.PostTypeID) as Color,
                                (select Rank from UserGroups where UserID = p.UserID and GroupID = p.GroupID) as Rank
                                FROM Posts p
                                WHERE p.GroupID IN 
	                                (SELECT ug.GroupID FROM UserGroups ug
	                                WHERE ug.UserID=@uid)";


            SqlCommand command = new SqlCommand(queryString, sqlConnection);
            command.Parameters.Add(new SqlParameter("@uid", userid));
            SqlDataReader reader = command.ExecuteReader();
            try
            {
                while (reader.Read())
                {
                    int rate;
                    if (reader.IsDBNull(11))
                        rate = 0;
                    else
                        rate = (int)reader[11];
                    postsList.Add(new RequestPost((int)reader[0], (string)reader[1], (DateTime)reader[2], (int)reader[3], (string)reader[4], (int)reader[5], (string)reader[6], (int)reader[7], (string)reader[8], (string)reader[9], (int)reader[10], rate));
                }
            }
            finally
            {
                // Always call Close when done reading.
                reader.Close();
            }
            sqlConnection.Close();
            return postsList;
        }

        public bool AddUserToGroup(string username, string groupname)
        {
            List<RequestGroup> list = new List<RequestGroup>();
            list = GetUserGroups(username);
            foreach(var rg in list)
            {
                if(rg.GroupName.Equals(groupname))
                {
                    return false;
                }
            }
            sqlConnection.Open();

            string uidString = "SELECT u.UserID FROM Users u WHERE u.Login=@uname";
            SqlCommand command1 = new SqlCommand(uidString, sqlConnection);
            command1.Parameters.Add(new SqlParameter("@uname", username));
            SqlDataReader reader1 = command1.ExecuteReader();
            reader1.Read();
            int uid = (int)reader1[0];
            reader1.Close();

            string gidString = "SELECT g.GroupID FROM Groups g WHERE g.GroupName=@uname";
            SqlCommand command2 = new SqlCommand(gidString, sqlConnection);
            command2.Parameters.Add(new SqlParameter("@uname", groupname));
            SqlDataReader reader2 = command2.ExecuteReader();
            reader2.Read();
            int gid = (int)reader2[0];
            reader2.Close();

           
            string queryString = "INSERT INTO UserGroups VALUES(@uid, @gid, 0)";
            SqlCommand command = new SqlCommand(queryString, sqlConnection);
            command.Parameters.Add(new SqlParameter("@uid", uid));
            command.Parameters.Add(new SqlParameter("@gid", gid));
            command.ExecuteNonQuery();
            sqlConnection.Close();
            return true;

        }

        public List<RequestGroup> NotGetUserGroups(string username)
        {
            sqlConnection.Open();
            List<RequestGroup> groups = new List<RequestGroup>();
            string queryString = @"select * from Groups
                                except
                                (select groupid, 
                                (select groupname from groups where groups.groupid = usergroups.groupid) from usergroups 
                                where userid = (select userid from users where login = @un))";
            SqlCommand command = new SqlCommand(queryString, sqlConnection);
            command.Parameters.Add(new SqlParameter("@un", username));
            SqlDataReader reader = command.ExecuteReader();
            try
            {
                while (reader.Read())
                {
                    groups.Add(new RequestGroup((int)reader[0], (string)reader[1]));
                }
            }
            finally
            {
                // Always call Close when done reading.
                reader.Close();
            }
            sqlConnection.Close();
            return groups;

        }

        public List<RequestGroup> GetUserGroups(string username)
        {
            sqlConnection.Open();
            List<RequestGroup> groups = new List<RequestGroup>();
            string queryString = @"select groupid, (select groupname from groups where groups.groupid = usergroups.groupid) from usergroups where userid = (select userid from users where login = @un)";

            SqlCommand command = new SqlCommand(queryString, sqlConnection);
            command.Parameters.Add(new SqlParameter("@un", username));
            SqlDataReader reader = command.ExecuteReader();
            try
            {
                while (reader.Read())
                {
                    groups.Add(new RequestGroup((int)reader[0], (string)reader[1]));
                }
            }
            finally
            {
                // Always call Close when done reading.
                reader.Close();
            }
            sqlConnection.Close();
            return groups;
        }

        public List<RequestRank> GetUserRanks(string username)
        {
            sqlConnection.Open();

            string updatestring = @"
                                    update usergroups set rank = 
                                    (
                                    case when
                                    (select avg(points) from posts
                                    where userid  = (select userid from users where login = @un)
                                    and groupid = usergroups.groupid)
                                    is null then 0 else
                                    (select avg(points) from posts
                                    where userid  = (select userid from users where login = @un)
                                    and groupid = usergroups.groupid)
                                    end
                                    )
                                    where userid = (select userid from users where login = @un)
                                ";

            SqlCommand updatecommand = new SqlCommand(updatestring, sqlConnection);
            updatecommand.Parameters.Add(new SqlParameter("@un", username));
            updatecommand.ExecuteNonQuery();

            List<RequestRank> ranks = new List<RequestRank>();
            string queryString = @"SELECT (SELECT g.GroupName FROM Groups g WHERE g.GroupID = UserGroups.GroupID), Rank FROM UserGroups
                                    WHERE UserID=(SELECT UserID FROM Users WHERE Login = @un)";

            SqlCommand command = new SqlCommand(queryString, sqlConnection);
            command.Parameters.Add(new SqlParameter("@un", username));
            SqlDataReader reader = command.ExecuteReader();
            try
            {
                while (reader.Read())
                {
                    ranks.Add(new RequestRank((string)reader[0], (int)reader[1]));
                }
            }
            finally
            {
                // Always call Close when done reading.
                reader.Close();
            }
            sqlConnection.Close();
            return ranks;
        }

        public List<RequestPost> GetAllUserPosts(string username)
        {
            sqlConnection.Open();
            List<RequestPost> postsList = new List<RequestPost>();
            string queryString = @"SELECT p.PostID, p.Text, p.ModifiedAt, p.UserID,
                                    (select Login from Users where UserID = p.UserID) as Login, 
                                    p.GroupID, 
                                    (select GroupName from Groups where GroupId = p.GroupId) as GroupName, 
                                    p.Points,
                                    (select TypeName from PostTypes where PostTypeID = p.PostTypeID) as TypeName, 
                                    p.Topic,
                                    (select Color from PostTypes where PostTypeID = p.PostTypeID) as Color,
                                    (select Rank from UserGroups where UserID = p.UserID and GroupID = p.GroupID) as Rank
                                    FROM Posts p
                                    WHERE p.UserID=(SELECT UserID FROM Users WHERE Login = @un)";
            
            SqlCommand command = new SqlCommand(queryString, sqlConnection);
            command.Parameters.Add(new SqlParameter("@un", username));
            SqlDataReader reader = command.ExecuteReader();
            try
            {
                while (reader.Read())
                {
                    int id = (int)reader[0];
                    string content = (string)reader[1];
                    DateTime mAt = (DateTime)reader[2];
                    int uid = (int)reader[3];
                    string login = (string)reader[4];
                    int gid = (int)reader[5];
                    string gname = (string)reader[6];
                    int points = (int)reader[7];
                    string type = (string)reader[8];
                    string topic = (string)reader[9];
                    int color = (int)reader[10];
                    int rate;
                    if (reader.IsDBNull(11))
                        rate = 0;
                    else
                        rate = (int)reader[11];
                    
                    postsList.Add(new RequestPost(id, content, mAt, uid, login, gid, gname, points, type, topic, color, rate));
                }
            }
            finally
            {
                // Always call Close when done reading.
                reader.Close();
            }
            sqlConnection.Close();
            return postsList;
        }



        public List<RequestType> GetAllPostTypes()
        {
            sqlConnection.Open();
            List<RequestType> typesList = new List<RequestType>();
            SqlCommand command = new SqlCommand("SELECT pt.TypeName, pt.Color FROM PostTypes pt", sqlConnection);
            SqlDataReader reader = command.ExecuteReader();
            try
            {
                while (reader.Read())
                {
                    typesList.Add(new RequestType((string)reader[0], (int)reader[1]));
                }
            }
            finally
            {
                // Always call Close when done reading.
                reader.Close();
            }
            sqlConnection.Close();
            return typesList;
        }

        public bool AddPostRate(int userId, int postId, RatePost data)
        {
            sqlConnection.Open();
            string checkString = "SELECT * FROM PostRates pr WHERE pr.PostID=@pid and pr.UserID=@uid";
            SqlCommand command = new SqlCommand(checkString, sqlConnection);
            command.Parameters.Add(new SqlParameter("@pid", postId));
            command.Parameters.Add(new SqlParameter("@uid", userId));
            SqlDataReader reader = command.ExecuteReader();
            if(reader.HasRows)
            {
                sqlConnection.Close();
                return false;
            }
            reader.Close();
            string queryString = "INSERT INTO PostRates VALUES(@uid, @pid, @vote)";
            SqlCommand command2 = new SqlCommand(queryString, sqlConnection);
            command2.Parameters.Add(new SqlParameter("@uid", userId));
            command2.Parameters.Add(new SqlParameter("@pid", postId));
            command2.Parameters.Add(new SqlParameter("@vote", (data.Value>0)));
            command2.ExecuteNonQuery();

            string queryString2 = @"UPDATE Posts SET Points = (case when 
                                    (select SUM(CASE WHEN IsVoteUp = 0 THEN -1 ELSE 1 END) 
                                    FROM PostRates
                                    WHERE PostRates.PostID = Posts.PostId)
                                    is null then 0 else
                                    (select SUM(CASE WHEN IsVoteUp = 0 THEN -1 ELSE 1 END) 
                                    FROM PostRates
                                    WHERE PostRates.PostID = Posts.PostId)
                                    end)
                                     WHERE PostId = @pid";
            SqlCommand command3 = new SqlCommand(queryString2, sqlConnection);
            command3.Parameters.Add(new SqlParameter("@pid", postId));
            command3.ExecuteNonQuery();

            sqlConnection.Close();
            return true;
        }

        public List<RequestPost> GetPostsFromGroup(string groupname)
        {
            List<RequestPost> postsList = new List<RequestPost>();
            sqlConnection.Open();
            string queryString = @"SELECT p.PostID, p.Text, p.ModifiedAt, p.UserID, 
                                (select Login from Users where UserID = p.UserID) as Login, 
                                p.GroupID, 
                                (select GroupName from Groups where GroupId = p.GroupId) as GroupName, 
                                p.Points, 
                                (select TypeName from PostTypes where PostTypeID = p.PostTypeID) as TypeName, 
                                p.Topic, 
                                (select Color from PostTypes where PostTypeID = p.PostTypeID) as Color, 
                                (select Rank from UserGroups where UserID = p.UserID and GroupID = p.GroupID) as Rank 
                                FROM Posts p
                                WHERE p.GroupID = (SELECT GroupID FROM Groups g
					                                WHERE g.GroupName=@gname)";
            SqlCommand command = new SqlCommand(queryString, sqlConnection);
            command.Parameters.Add(new SqlParameter("@gname", groupname));
            SqlDataReader reader = command.ExecuteReader();
            try
            {
                while (reader.Read())
                {
                    int rate;
                    if (reader.IsDBNull(11))
                        rate = 0;
                    else
                        rate = (int)reader[11];
                    postsList.Add(new RequestPost((int)reader[0], (string)reader[1], (DateTime)reader[2], (int)reader[3], (string)reader[4], (int)reader[5], (string)reader[6], (int)reader[7], (string)reader[8], (string)reader[9], (int)reader[10], rate));
                }
            }
            finally
            {
                // Always call Close when done reading.
                reader.Close();
            }
            sqlConnection.Close();
            return postsList;
        }

        public List<string> GetAllUserTypes()
        {
            sqlConnection.Open();
            List<string> typesList = new List<string>();
            SqlCommand command = new SqlCommand("SELECT ut.Name FROM UserTypes ut", sqlConnection);
            SqlDataReader reader = command.ExecuteReader();
            try
            {
                while (reader.Read())
                {
                    typesList.Add((string)reader[0]);
                }
            }
            finally
            {
                // Always call Close when done reading.
                reader.Close();
            }
            sqlConnection.Close();
            return typesList;
        }

        public bool AddCommentRate(int userId, int commentId, RateComment data)
        {
            sqlConnection.Open();

            string postidQuery = "SELECT c.PostID FROM Comments c WHERE c.CommID = @cid";
            SqlCommand comm = new SqlCommand(postidQuery, sqlConnection);
            comm.Parameters.Add(new SqlParameter("@cid", commentId));
            SqlDataReader read = comm.ExecuteReader();
            if (read.HasRows)
            {
                sqlConnection.Close();
                return false;
            }
            read.Read();
            int pID = (int)read[0];
            read.Close();

            string checkString = "SELECT * FROM CommRates cr WHERE cr.CommID=@cid and cr.UserID=@uid";
            SqlCommand command = new SqlCommand(checkString, sqlConnection);
            command.Parameters.Add(new SqlParameter("@cid", commentId));
            command.Parameters.Add(new SqlParameter("@uid", userId));
            SqlDataReader reader = command.ExecuteReader();
            if (reader.HasRows)
            {
                sqlConnection.Close();
                return false;
            }
            reader.Close();
            string queryString = "INSERT INTO PostRates pr VALUES(@cid, @pid, @vote, @pid)";
            SqlCommand command2 = new SqlCommand(queryString, sqlConnection);
            command2.Parameters.Add(new SqlParameter("@cid", commentId));
            command2.Parameters.Add(new SqlParameter("@uid", userId));
            command2.Parameters.Add(new SqlParameter("@vote", (data.Value > 0)));
            command2.Parameters.Add(new SqlParameter("@pid", pID));
            command2.ExecuteNonQuery();

            string queryString2 = @"UPDATE Comments SET Points = (select SUM(CASE WHEN IsVoteUp = 0 THEN -1 ELSE 1 END) FROM CommRates
                                    WHERE CommRates.CommentID = Comments.CommentID) WHERE CommentID = @cid";
            SqlCommand command3 = new SqlCommand(queryString2, sqlConnection);
            command3.Parameters.Add(new SqlParameter("@cid", commentId));
            command3.ExecuteNonQuery();

            sqlConnection.Close();
            return true;
        }

        public RequestUser VerifyUser(AuthenticationInfo authInfo)
        {
            sqlConnection.Open();
            string queryString = "SELECT u.Login, u.FirstName, u.LastName, u.CreateAt, u.Password  FROM Users u WHERE u.Login=@login";
            SqlCommand command = new SqlCommand(queryString, sqlConnection);
            command.Parameters.Add(new SqlParameter("@login", authInfo.Login));
            SqlDataReader reader = command.ExecuteReader();
            if(reader.HasRows==false)
            {
                sqlConnection.Close();
                return null;
            }

            reader.Read();
            if (((string)reader[4]).Equals(CalculateMD5Hash(authInfo.Password))==false)
            {
                sqlConnection.Close();
                return null;
            }
            RequestUser tmp = new RequestUser((string)reader[0], (string)reader[1], (string)reader[2], (DateTime)reader[3]);
            sqlConnection.Close();
            return tmp;
        }

        public bool AddPost(AddPost addPost, int userID)
        {
            sqlConnection.Open();
            string checkString = "SELECT t.PostTypeID FROM PostTypes t WHERE t.TypeName=@tname";
            SqlCommand command = new SqlCommand(checkString, sqlConnection);
            command.Parameters.Add(new SqlParameter("@tname", addPost.Type));
            SqlDataReader reader = command.ExecuteReader();
            if(reader.HasRows==false)
            {
                sqlConnection.Close();
                return false;
            }
            reader.Read();
            int typeid = (int)reader[0];
            reader.Close();
            string gnameString = "SELECT g.GroupID FROM Groups g WHERE g.GroupName=@gname";
            SqlCommand command2 = new SqlCommand(gnameString, sqlConnection);
            command2.Parameters.Add(new SqlParameter("@gname", addPost.GroupName));
            SqlDataReader reader2 = command2.ExecuteReader();
            if (reader2.HasRows == false)
            {
                sqlConnection.Close();
                return false;
            }
            reader2.Read();
            int gid = (int)reader2[0];
            reader2.Close();
            
            string queryString = "INSERT INTO Posts VALUES(@content, @date, @date, @uID, @gID, 0, @typeid, @tpc)";
            SqlCommand command3 = new SqlCommand(queryString, sqlConnection);
            command3.Parameters.Add(new SqlParameter("@content", addPost.Content));
            command3.Parameters.Add(new SqlParameter("@date", DateTime.Now));
            command3.Parameters.Add(new SqlParameter("@uID", userID.ToString()));
            command3.Parameters.Add(new SqlParameter("@gID", gid));
            command3.Parameters.Add(new SqlParameter("@typeid", typeid));
            command3.Parameters.Add(new SqlParameter("@tpc", addPost.Title));
            command3.ExecuteNonQuery();
            sqlConnection.Close();
            return true;

        }

        public RequestPost GetPost(int PostID)
        {
            sqlConnection.Open();
            string queryString = @"SELECT p.Text, p.ModifiedAt, p.UserID, u.Login, p.GroupID, g.GroupName, p.Points, t.TypeName, p.Topic, t.Color, ug.Rank FROM Posts p 
                                    JOIN Users u ON u.UserID=p.UserID
                                    JOIN UserGroups ug ON u.UserID=ug.UserID
                                    JOIN Groups g ON g.GroupID=p.GroupID
                                    JOIN PostTypes t ON t.PostTypeID=p.PostTypeID
                                    WHERE p.PostID=@pid";
            SqlCommand command = new SqlCommand(queryString, sqlConnection);
            command.Parameters.Add(new SqlParameter("@pid", PostID));
            SqlDataReader reader = command.ExecuteReader();
            if (reader.HasRows == false)
            {
                sqlConnection.Close();
                return null;
            }
            reader.Read();
            RequestPost requestPost = new RequestPost(PostID, (string)reader[0], (DateTime)reader[1], (int)reader[2], (string)reader[3], (int)reader[4], (string)reader[5], (int)reader[6], (string)reader[7], (string)reader[8], (int)reader[9], (int)reader[10]);
            sqlConnection.Close();
            return requestPost;
        }

        public bool AddUser(AddUser addUser)
        {
            sqlConnection.Open();
            string checkString = "SELECT * FROM Users u WHERE u.Login=@login";
            SqlCommand command = new SqlCommand(checkString, sqlConnection);
            command.Parameters.Add(new SqlParameter("@login", addUser.Login));
            SqlDataReader reader = command.ExecuteReader();
            if(reader.HasRows==true)
            {
                sqlConnection.Close();
                return false;
            }
            reader.Close();
            string typeidString = "SELECT t.TypeID FROM UserTypes t WHERE t.Name=@tname";
            SqlCommand command2 = new SqlCommand(typeidString, sqlConnection);
            command2.Parameters.Add(new SqlParameter("@tname", addUser.Type));
            SqlDataReader reader2 = command2.ExecuteReader();
            if(reader2.HasRows==false)
            {
                sqlConnection.Close();
                return false;
            }
            reader2.Read();
            int typeid = (int)reader2[0];
            reader2.Close();

            string queryString = "INSERT INTO Users VALUES(@login, @pwd, @name, @lastname, @dt, null, @tid)";
            SqlCommand command3 = new SqlCommand(queryString, sqlConnection);
            command3.Parameters.Add(new SqlParameter("@login", addUser.Login));
            command3.Parameters.Add(new SqlParameter("@pwd", CalculateMD5Hash(addUser.Password)));
            command3.Parameters.Add(new SqlParameter("@name", addUser.FirstName));
            command3.Parameters.Add(new SqlParameter("@lastname", addUser.LastName));
            command3.Parameters.Add(new SqlParameter("@dt", DateTime.Now));
            command3.Parameters.Add(new SqlParameter("@tid", typeid));
            command3.ExecuteNonQuery();
            sqlConnection.Close();
            return true;
        }

        public List<RequestComment> GetComments(int PostID)
        {
            sqlConnection.Open();
            List<RequestComment> comList = new List<RequestComment>();

            string queryString = @"SELECT c.CommentID, c.Text, c.ModifiedAt, c.UserID,
                                (select Login from Users where UserID = c.UserID) as Login,
                                  c.Points,
                                (select Rank from UserGroups where UserID = c.UserID and GroupID = (select GroupId from Posts where PostID = c.PostID)) as Rank
                                  FROM Comments c
                                  WHERE c.PostID=@pid
                                  ORDER BY c.ModifiedAt";
            SqlCommand command = new SqlCommand(queryString, sqlConnection);
            command.Parameters.Add(new SqlParameter("@pid", PostID));
            SqlDataReader reader = command.ExecuteReader();
            if(reader.HasRows==false)
            {
                sqlConnection.Close();
                return null;
            }
            try
            {
                while (reader.Read())
                {
                    int rate;
                    if (reader.IsDBNull(6))
                        rate = 0;
                    else
                        rate = (int)reader[6];
                    comList.Add(new RequestComment((int)reader[0], (string)reader[1], (DateTime)reader[2], (int)reader[3], (string)reader[4], (int)reader[5], rate));
                }
            }
            finally
            {
                // Always call Close when done reading.
                reader.Close();
            }
            sqlConnection.Close();
            return comList;
        }


        public bool AddComment(AddComment addComment, int userId)
        {
            sqlConnection.Open();
            string checkString = "SELECT * FROM Posts p WHERE p.PostID=@pid";
            SqlCommand command = new SqlCommand(checkString, sqlConnection);
            command.Parameters.Add(new SqlParameter("@pid", addComment.PostId));
            SqlDataReader reader = command.ExecuteReader();
            if (reader.HasRows == false)
            {
                sqlConnection.Close();
                return false;
            }
            reader.Close();
            string queryString = "INSERT INTO Comments VALUES(@pid, 0, @uid, @dt, @dt, @text)";
            SqlCommand command2 = new SqlCommand(queryString, sqlConnection);
            command2.Parameters.Add(new SqlParameter("@pid", addComment.PostId));
            command2.Parameters.Add(new SqlParameter("@uid", userId));
            command2.Parameters.Add(new SqlParameter("@dt", DateTime.Now));
            command2.Parameters.Add(new SqlParameter("@text", addComment.Content));
            command2.ExecuteNonQuery();
            sqlConnection.Close();
            return true;
        }

        public int GetUserId(string login)
        {
            sqlConnection.Open();
            string queryString = "SELECT u.UserID FROM Users u WHERE u.Login=@login";
            SqlCommand command = new SqlCommand(queryString, sqlConnection);
            command.Parameters.Add(new SqlParameter("@login", login));
            SqlDataReader reader = command.ExecuteReader();
            if (reader.HasRows == false)
            {
                sqlConnection.Close();
                return -1;
            }
            reader.Read();
            int userID = (int)reader[0];
            sqlConnection.Close();
            return userID;
        }

        private string CalculateMD5Hash(string input)
        {
            // step 1, calculate MD5 hash from input

            MD5 md5 = System.Security.Cryptography.MD5.Create();

            byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(input);

            byte[] hash = md5.ComputeHash(inputBytes);

            // step 2, convert byte array to hex string

            StringBuilder sb = new StringBuilder();

            for (int i = 0; i < hash.Length; i++)

            {
                sb.Append(hash[i].ToString("x2"));
            }

            return sb.ToString();
        }

    }
}