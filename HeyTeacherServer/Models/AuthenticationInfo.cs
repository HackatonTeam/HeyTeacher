﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HeyTeacherServer.Models
{
    public class AuthenticationInfo
    {
        public string Login { get; protected set; }
        public string Password { get; protected set; }

        public AuthenticationInfo(string login, string password)
        {
            Login = login;
            Password = password;
        }
    }
}