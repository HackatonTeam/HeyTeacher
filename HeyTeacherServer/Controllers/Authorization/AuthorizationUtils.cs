﻿using HeyTeacherServer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Headers;
using System.Web;

namespace HeyTeacherServer.Controllers.Authorization
{
    public static class AuthorizationUtils
    {
        public static AuthenticationInfo ParseAuthHeader(AuthenticationHeaderValue header)
        {
            if (header == null || header.Scheme != "Basic")
            {
                return null;
            }
            string[] data = null;
            try
            {
                data = DecodeB64(header.Parameter).Split(':');
            }
            catch (Exception)
            {
                return null;
            }

            if (data.Length != 2)
            {
                return null;
            }

            return new AuthenticationInfo(data[0], data[1]);
        }

        public static string DecodeB64(string b64String)
        {
            return System.Text.Encoding.UTF8.GetString(System.Convert.FromBase64String(b64String));
        }
    }
}