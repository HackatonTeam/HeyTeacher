﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;
using HeyTeacherCommon.CommunicationModels;
using HeyTeacherCommon.CommunicationModels.CommunicationModels;
using HeyTeacherServer.Controllers.Authorization;
using HeyTeacherServer.Models;
using Newtonsoft.Json;

namespace HeyTeacherServer.Controllers
{
    public class UserController : ApiController
    {
        [HttpPost]
        [Route("users")]
        public HttpResponseMessage LogUser()
        {
            AuthenticationInfo authInfo = AuthorizationUtils.ParseAuthHeader(Request.Headers.Authorization);
            if (authInfo != null)
            {
                RequestUser user = Database.Database.Instance.VerifyUser(authInfo);
                if (user != null)
                {
                    return Request.CreateResponse(HttpStatusCode.OK, user);
                }
            }
            return Request.CreateResponse(HttpStatusCode.Forbidden);
        }

        [HttpPut]
        [Route("users")]
        public HttpResponseMessage AddUser(AddUser user)
        {
            if(user == null)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }
            bool success = Database.Database.Instance.AddUser(user);
            if (success)
            {
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            return Request.CreateResponse(HttpStatusCode.Conflict);
        }

        [HttpGet]
        [Route("users/{username}")]
        public HttpResponseMessage GetUserByLogin([FromUri] string username)
        {
            if (String.IsNullOrEmpty(username))
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }
            RequestUser user = Database.Database.Instance.GetUserFromLogin(username);
            if(user == null)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }
            return Request.CreateResponse(HttpStatusCode.OK, user);
        }

        [HttpGet]
        [Route("users/{username}/ranks")]
        public HttpResponseMessage GetUserRanks([FromUri] string username)
        {
            if (String.IsNullOrEmpty(username))
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }
            List<RequestRank> ranks = Database.Database.Instance.GetUserRanks(username);
            if(ranks == null)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }
            return Request.CreateResponse(HttpStatusCode.OK, ranks);
        }

        [HttpGet]
        [Route("usertypes")]
        public HttpResponseMessage GetUserTypes()
        {
            List<string> types = Database.Database.Instance.GetAllUserTypes();
            return Request.CreateResponse(HttpStatusCode.OK, types);
        }

        [HttpGet]
        [Route("usergroups/{username}")]
        public HttpResponseMessage GetUserGroups([FromUri] string username)
        {
            if (String.IsNullOrEmpty(username))
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }
            List<RequestGroup> groups = Database.Database.Instance.GetUserGroups(username);
            return Request.CreateResponse(HttpStatusCode.OK, groups);
        }

        [HttpGet]
        [Route("nonusergroups/{username}")]
        public HttpResponseMessage GetNonUserGroups([FromUri] string username)
        {
            if (String.IsNullOrEmpty(username))
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }
            List<RequestGroup> groups = Database.Database.Instance.NotGetUserGroups(username);
            return Request.CreateResponse(HttpStatusCode.OK, groups);
        }

        [HttpPut]
        [Route("usergroups/{username}/{groupname}")]
        public HttpResponseMessage AddUserToGroup([FromUri] string username, [FromUri] string groupname)
        {
            if (String.IsNullOrEmpty(username) || String.IsNullOrEmpty(groupname))
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }
            Database.Database.Instance.AddUserToGroup(username, groupname);
            return Request.CreateResponse(HttpStatusCode.OK);
        }
    }
}
