﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using HeyTeacherCommon.CommunicationModels;
using HeyTeacherServer.Controllers.Authorization;
using HeyTeacherServer.Models;
using Newtonsoft.Json;

namespace HeyTeacherServer.Controllers
{
    public class PostController : ApiController
    {
        [HttpPut]
        [Route("posts")]
        public HttpResponseMessage AddPost([FromBody] AddPost data)
        {
            if (data == null)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }
            AuthenticationInfo authInfo = AuthorizationUtils.ParseAuthHeader(Request.Headers.Authorization);
            if (authInfo != null)
            {
                if (Database.Database.Instance.VerifyUser(authInfo) != null) {
                    int userId = Database.Database.Instance.GetUserId(authInfo.Login);
                    Database.Database.Instance.AddPost(data, userId);
                    return Request.CreateResponse(HttpStatusCode.OK);
                }
            }
            return Request.CreateResponse(HttpStatusCode.Forbidden);
        }

        [HttpGet]
        [Route("posts/{postId}")]
        public HttpResponseMessage GetPost([FromUri] int postId)
        {
            RequestPost post = Database.Database.Instance.GetPost(postId);
            if(post == null)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }
            return Request.CreateResponse(HttpStatusCode.OK, post);
        }

        [HttpGet]
        [Route("posts/{postId}/comments")]
        public HttpResponseMessage GetComments([FromUri] int postId)
        {
            List<RequestComment> comments = Database.Database.Instance.GetComments(postId);
            if(comments == null)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }
            return Request.CreateResponse(HttpStatusCode.OK, comments);
        }

        [HttpPut]
        [Route("posts/{postId}/comments")]
        public HttpResponseMessage AddComment([FromUri] int postId, [FromBody] AddComment data)
        {
            if (data == null)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }
            AuthenticationInfo authInfo = AuthorizationUtils.ParseAuthHeader(Request.Headers.Authorization);
            if (authInfo != null)
            {
                if (Database.Database.Instance.VerifyUser(authInfo) != null)
                {
                    int userId = Database.Database.Instance.GetUserId(authInfo.Login);
                    bool success = Database.Database.Instance.AddComment(data, userId);
                    if (success == false)
                    {
                        return Request.CreateResponse(HttpStatusCode.NotFound);
                    }
                    return Request.CreateResponse(HttpStatusCode.OK);
                }
            }
            return Request.CreateResponse(HttpStatusCode.Forbidden);
        }

        [HttpGet]
        [Route("types")]
        public HttpResponseMessage GetAllTypes()
        {
            List<RequestType> types = Database.Database.Instance.GetAllPostTypes();
            return Request.CreateResponse(HttpStatusCode.OK, types);
        }

        [HttpGet]
        [Route("posts")]
        public HttpResponseMessage GetPostsForUser()
        {
            AuthenticationInfo authInfo = AuthorizationUtils.ParseAuthHeader(Request.Headers.Authorization);
            if (authInfo != null)
            {
                if (Database.Database.Instance.VerifyUser(authInfo) != null)
                {
                    int userId = Database.Database.Instance.GetUserId(authInfo.Login);
                    List<RequestPost> posts = Database.Database.Instance.GetAllPostsForUser(userId);
                    if(posts == null)
                    {
                        return Request.CreateResponse(HttpStatusCode.NotFound);
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, posts);
                }
            }
            return Request.CreateResponse(HttpStatusCode.Forbidden);
        }

        [HttpGet]
        [Route("posts")]
        public HttpResponseMessage GetAllPostsByUser([FromUri] string username)
        {
            List<RequestPost> posts = Database.Database.Instance.GetAllUserPosts(username);
            if(posts == null)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }
            return Request.CreateResponse(HttpStatusCode.OK, posts);
        }

        [HttpPost]
        [Route("posts/{postId}")]
        public HttpResponseMessage RatePost([FromUri] int postId, [FromBody] RatePost data)
        {
            if (data == null)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }
            AuthenticationInfo authInfo = AuthorizationUtils.ParseAuthHeader(Request.Headers.Authorization);
            if (authInfo != null)
            {
                if (Database.Database.Instance.VerifyUser(authInfo) != null)
                {
                    int userId = Database.Database.Instance.GetUserId(authInfo.Login);
                    bool success = Database.Database.Instance.AddPostRate(userId, postId, data);
                    if (success)
                    {
                        return Request.CreateResponse(HttpStatusCode.OK);
                    }
                    return Request.CreateResponse(HttpStatusCode.NotFound);
                }
            }
            return Request.CreateResponse(HttpStatusCode.Forbidden);
        }

        [HttpPost]
        [Route("comments/{commentId}")]
        public HttpResponseMessage RateComment([FromUri] int commentId, [FromBody] RateComment data)
        {
            if (data == null)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }
            AuthenticationInfo authInfo = AuthorizationUtils.ParseAuthHeader(Request.Headers.Authorization);
            if (authInfo != null)
            {
                if (Database.Database.Instance.VerifyUser(authInfo) != null)
                {
                    int userId = Database.Database.Instance.GetUserId(authInfo.Login);
                    bool success = Database.Database.Instance.AddCommentRate(userId, commentId, data);
                    if (success)
                    {
                        return Request.CreateResponse(HttpStatusCode.OK);
                    }
                    return Request.CreateResponse(HttpStatusCode.NotFound);
                }
            }
            return Request.CreateResponse(HttpStatusCode.Forbidden);
        }

        [HttpGet]
        [Route("posts")]
        public HttpResponseMessage GetPostsByGroup([FromUri] string groupname)
        {
            if (String.IsNullOrEmpty(groupname))
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }
            List<RequestPost> posts = Database.Database.Instance.GetPostsFromGroup(groupname);
            if(posts == null)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }
            return Request.CreateResponse(HttpStatusCode.OK, posts);
        }
    }
}
