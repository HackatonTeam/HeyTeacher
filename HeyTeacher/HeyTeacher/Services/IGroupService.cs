﻿using HeyTeacher.Models;
using HeyTeacherCommon.CommunicationModels;
using HeyTeacherCommon.CommunicationModels.CommunicationModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace HeyTeacher.Services
{
    interface IGroupService
    {
        Task<List<RequestGroup>> GetUserGroups(string userName);

        Task<List<RequestGroup>> GetOtherGroups(string userName);

        Task<List<Post>> GetPostsByGroupName(string groupName);

        Task JoinGroup(string groupName, string userName);
    }
}
