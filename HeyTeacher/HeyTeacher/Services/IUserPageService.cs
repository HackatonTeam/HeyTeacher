﻿using HeyTeacherCommon.CommunicationModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace HeyTeacher.Services
{
    interface IUserPageService
    {
        Task<RequestUser> GetUserByLogin(string userLogin);
    }
}
