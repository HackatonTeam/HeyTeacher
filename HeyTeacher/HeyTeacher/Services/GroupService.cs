﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using HeyTeacher.Models;
using HeyTeacherCommon.CommunicationModels;
using HeyTeacherCommon.CommunicationModels.CommunicationModels;

namespace HeyTeacher.Services
{
    class GroupService : IGroupService
    {
        private IHttpClientService _httpClientService;

        public GroupService(IHttpClientService httpClientService)
        {
            _httpClientService = httpClientService;
        }

        public async Task<List<RequestGroup>> GetOtherGroups(string userName)
        {
            string token = _httpClientService.GenerateAuthenticationToken();
            return await _httpClientService.GetAsync<List<RequestGroup>>("usergroups/" + userName, token);
        }

        public async Task<List<RequestGroup>> GetUserGroups(string userName)
        {
            string token = _httpClientService.GenerateAuthenticationToken();
            return await _httpClientService.GetAsync<List<RequestGroup>>("nonusergroups/" + userName, token);
        }

        public async Task<List<Post>> GetPostsByGroupName(string groupName)
        {
            List<RequestPost> requestPosts = await _httpClientService.GetAsync<List<RequestPost>>("posts?groupname=" + groupName, null);
            List<Post> postsList = new List<Post>();
            foreach (RequestPost rp in requestPosts)
                postsList.Add(new Post(rp));
            return postsList;
        }

        public async Task JoinGroup(string groupName, string userName)
        {
            await _httpClientService.PutAsync<string>("usergroups/" + userName + "/" + groupName, null, null);
        }
    }
}
