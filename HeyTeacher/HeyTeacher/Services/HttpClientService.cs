﻿using HeyTeacher.Models;
using HeyTeacherCommon.CommunicationModels;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;

using System.Text;
using System.Threading.Tasks;

namespace HeyTeacher.Services
{
    public class HttpClientService : IHttpClientService
    {
        private const string SERVER_HOST = "http://heyteacherserver.azurewebsites.net";
        private const string CONTENT_TYPE = "application/json";
        private const string RANKS = "ranks";
        private Uri baseUri;
        private readonly JsonSerializerSettings _serializerSettings;

        public User LoggedUser { get; set; }

        public HttpClientService()
        {
            baseUri = new Uri(SERVER_HOST);
            _serializerSettings = new JsonSerializerSettings
            {
                ContractResolver = new CamelCasePropertyNamesContractResolver(),
                DateFormatHandling = DateFormatHandling.IsoDateFormat,
                NullValueHandling = NullValueHandling.Ignore
            };
            _serializerSettings.Converters.Add(new StringEnumConverter());
        }

        public async Task<T> GetAsync<T>(string path, string token)
        {
            Uri uri = new Uri(baseUri, path);
            HttpClient httpClient = PrepareHttpClient(token);
            HttpResponseMessage response = await httpClient.GetAsync(uri);

            ValidateResponse(response);
            string responseContent = await response.Content.ReadAsStringAsync();
            return await ParseContent<T>(responseContent);
        }

        public async Task<T> PostAsync<T>(string path, object data, string token)
        {
            Uri uri = new Uri(baseUri, path);
            HttpClient httpClient = PrepareHttpClient(token);
            var requestContent = SerializeObject(data);
            HttpResponseMessage response = await httpClient.PostAsync(uri, requestContent);

            ValidateResponse(response);
            string responseContent = await response.Content.ReadAsStringAsync();
            return await ParseContent<T>(responseContent);
        }

        public async Task<T> PutAsync<T>(string path, object data, string token)
        {
            Uri uri = new Uri(baseUri, path);
            HttpClient httpClient = PrepareHttpClient(token);
            var requestContent = SerializeObject(data);
            HttpResponseMessage response = await httpClient.PutAsync(uri, requestContent);

            ValidateResponse(response);
            string responseContent = await response.Content.ReadAsStringAsync();
            return await ParseContent<T>(responseContent);
        }

        public async Task<T> DeleteAsync<T>(string path, string token)
        {
            Uri uri = new Uri(baseUri, path);
            HttpClient httpClient = PrepareHttpClient(token);
            HttpResponseMessage response = await httpClient.DeleteAsync(uri);

            ValidateResponse(response);
            string responseContent = await response.Content.ReadAsStringAsync();
            return await ParseContent<T>(responseContent);
        }

        public async Task<string> CallRequest(string path, string method, string requestBody = "")
        {
            Uri uri = new Uri(baseUri, path);
            HttpClient httpClient = PrepareHttpClient();

            if (requestBody == null) requestBody = "";
            var content = new StringContent(requestBody);
            content.Headers.ContentType = new MediaTypeHeaderValue(CONTENT_TYPE);

            HttpResponseMessage response;
            switch (method)
            {
                case "GET":
                    response = await httpClient.GetAsync(uri);
                    break;
                case "POST":
                    response = await httpClient.PostAsync(uri, content);
                    break;
                case "PUT":
                    response = await httpClient.PutAsync(uri, content);
                    break;
                case "DELETE":
                    response = await httpClient.DeleteAsync(uri);
                    break;
                default:
                    response = await httpClient.GetAsync(uri);
                    break;
            }
            return await response.Content.ReadAsStringAsync();
        }

        private HttpClient PrepareHttpClient(string token = null)
        {
            HttpClient httpClient = new HttpClient();
            httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(CONTENT_TYPE));

            //prepare authentication
            if (!string.IsNullOrEmpty(token))
            {
                httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", token);
            }

            return httpClient;
        }

        public async Task<List<RequestRank>> GetUserRank(string username)
        {
            string path = "Users/" + username + "/" + RANKS;
            List<RequestRank> typesResponse = await GetAsync<List<RequestRank>>(path, null);
            return typesResponse;
        }

        public string GenerateAuthenticationToken(User user = null)
        {
            user = user ?? LoggedUser;
            if (user == null)
            {
                throw new Exception("you shoud set LoggedUser for ClientService in your ViewModel");
            }
            string authString = $"{user.Username}:{user.Password}";
            byte[] authBytes = Encoding.UTF8.GetBytes(authString.ToCharArray());
            string token = Convert.ToBase64String(authBytes);
            return token;
        }

        private void ValidateResponse(HttpResponseMessage response)
        {
            switch (response.StatusCode)
            {
                case HttpStatusCode.OK:
                    return;
                case HttpStatusCode.Forbidden:
                    throw new Exception("FORBIDDEN!");
                case HttpStatusCode.InternalServerError:
                    throw new Exception("Internal Server Error");
            }
        }

        private async Task<T> ParseContent<T>(string content)
        {
            //in the future this function should has try/catch block 
            //catch will map potentially JsonConverter exceptions to our custom exception
            T data;
            try
            {
                    data = await Task.Run(
                    () =>
                    JsonConvert.DeserializeObject<T>(content, _serializerSettings));
            }
            catch(Exception)
            {
                await App.Current.MainPage.DisplayAlert("Exp", content, "cancel");
                data = default(T);  
            }
            
            return data;
        }

        private StringContent SerializeObject(object data)
        {
            //probably it also should be wrapped in try/catch block in the future
            var content = new StringContent(JsonConvert.SerializeObject(data));
            content.Headers.ContentType = new MediaTypeHeaderValue(CONTENT_TYPE);
            return content;
        }

    }
}
