﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using HeyTeacher.Models;
using HeyTeacherCommon.CommunicationModels;

namespace HeyTeacher.Services
{
    class LoginService : ILoginService
    {
        private IHttpClientService _clientService;

        private const string LOGIN_USER = "users";
        private const string ADD_USER = "users";
        

        public LoginService(IHttpClientService clientService)
        {
            _clientService = clientService;
        }

        public async Task<User> Login(string user, string password)
        {
            User loggedUser = new User(user, password);
            string token = _clientService.GenerateAuthenticationToken(loggedUser);
            RequestUser userResponse = await _clientService.PostAsync<RequestUser>(LOGIN_USER, null, token);
            loggedUser.FirstName = userResponse.FirstName;
            loggedUser.LastName = userResponse.LastName;
            loggedUser.Created = userResponse.Created;
            _clientService.LoggedUser = loggedUser;
            return loggedUser;
        }

        public async Task<User> AddNewUser(string username, string password, string firstname, string lastname, string type)
        {
            User loggedUser = new User(username, password);
            AddUser user = new AddUser(username, password, firstname, lastname, type);
            RequestUser userResponse = await _clientService.PutAsync<RequestUser>(ADD_USER, user, null);
            loggedUser.FirstName = firstname;
            loggedUser.LastName = lastname;
            loggedUser.Created = DateTime.Now;
            _clientService.LoggedUser = loggedUser;
            return loggedUser;
        }
    }
}
