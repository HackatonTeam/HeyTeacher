﻿using HeyTeacher.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace HeyTeacher.Services
{
    interface ILoginService
    {
        Task<User> Login(string user, string password);

        Task<User> AddNewUser(string username, string password, string firstname, string lastname, string type);
    }
}
