﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using HeyTeacher.Models;
using HeyTeacherCommon.CommunicationModels;

namespace HeyTeacher.Services
{
    class PostService : IPostService
    {
        private IHttpClientService _clientService;

        private const string ADD_USER = "coments";
        private const string GET_ALL_POSTS = "posts";
        private const string GET_USER_POSTS = "posts";
        private const string ADD_COMMENT = "comments";
        private const string GET_POST = "posts";
        private const string GET_COMMENTS = "comments";

        public PostService(IHttpClientService clientService)
        {
            _clientService = clientService;
        }

        public async Task<List<Comment>> GetAllCommentsToPost(Post post)
        {
            List<RequestComment> comments = await _clientService.GetAsync<List<RequestComment>>(GET_POST + "/" + post.ID + "/" + GET_COMMENTS, null);
            List<Comment> commentsList = new List<Comment>();
            
            if (comments == null || comments.Count == 0)
                return new List<Comment>();

            foreach (RequestComment rc in comments)
                commentsList.Add(new Comment(rc));

            return commentsList;
        }

        public async Task<List<Post>> GetAllPosts()
        {
            string token = _clientService.GenerateAuthenticationToken();
            List<RequestPost> allPosts = await _clientService.GetAsync<List<RequestPost>>(GET_ALL_POSTS, token);
            List<Post> allPostsList = new List<Post>();
            foreach (RequestPost rp in allPosts)
                allPostsList.Add(new Post(rp));

            return allPostsList;
        }

        public async Task<List<Post>> GetAllUserPosts(User user)
        {
            string token = _clientService.GenerateAuthenticationToken();
            List<RequestPost> userPosts = await _clientService.GetAsync<List<RequestPost>>(GET_USER_POSTS + "?username=" + user.Username, token);
            List<Post> userPostsList = new List<Post>();
            foreach (RequestPost rp in userPosts)
                userPostsList.Add(new Post(rp));

            return userPostsList;
        }

        public async Task<Post> GetPostById(int id)
        {
            string token = _clientService.GenerateAuthenticationToken();
            Post post = await _clientService.GetAsync<Post>(GET_POST, token);
            return post;
        }

        public async Task AddNewComment(string content, int postId)
        {
            AddComment addComment = new AddComment(content, postId);
            string token = _clientService.GenerateAuthenticationToken();
            await _clientService.PutAsync<string>("posts/" + postId + "/comments", addComment, token);
        }

        public async Task AddPostRate(int postId, int value)
        {
            RatePost ratePost = new RatePost(value);
            string token = _clientService.GenerateAuthenticationToken();
            await _clientService.PostAsync<string>("posts/" + postId, ratePost, token);
            
        }

        public async Task AddCommentRate(int commentId, int value)
        {
            RateComment rateComment = new RateComment(value);
            string token = _clientService.GenerateAuthenticationToken();
            await _clientService.PostAsync<string>("comments/" + commentId, rateComment, token);
        }
    }
}
