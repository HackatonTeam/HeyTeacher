﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using HeyTeacher.Models;
using HeyTeacherCommon.CommunicationModels;

namespace HeyTeacher.Services
{
    class AddPostService : IAddPostService
    {
        private IHttpClientService _clientService;

        private const string TYPES = "types";
        private const string ADD_POST = "posts";

        public AddPostService(IHttpClientService clientService)
        {
            _clientService = clientService;
        }

        public async Task<List<RequestType>> GetAllPostTypes()
        {
            List<RequestType> typesResponse = await _clientService.GetAsync<List<RequestType>>(TYPES, null);
            return typesResponse;
        }
        public async void AddNewPost(Post post)
        {
            string token = _clientService.GenerateAuthenticationToken();
            AddPost addpost = Post.GetAddPostFromPost(post);
            await _clientService.PutAsync<string>(ADD_POST, addpost, token);
        }
    }
}
