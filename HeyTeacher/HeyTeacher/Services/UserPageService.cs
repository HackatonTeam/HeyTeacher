﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using HeyTeacherCommon.CommunicationModels;

namespace HeyTeacher.Services
{
    public class UserPageService : IUserPageService
    {
        private IHttpClientService _clientService;
        private string GET_USER = "users/";


        public UserPageService(IHttpClientService clientService)
        {
            _clientService = clientService;
        }

        public async Task<RequestUser> GetUserByLogin(string userLogin)
        {
            RequestUser requestUser = await _clientService.GetAsync<RequestUser>(GET_USER + userLogin, null);
            return requestUser;
        }
    }
}
