﻿using HeyTeacher.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace HeyTeacher.Services
{
    interface IPostService
    {
        Task<List<Post>> GetAllPosts();

        Task<List<Post>> GetAllUserPosts(User user);

        Task<List<Comment>> GetAllCommentsToPost(Post post);

        Task<Post> GetPostById(int id);
        
        Task AddNewComment(string content, int postId);

        Task AddPostRate(int id, int value);

        Task AddCommentRate(int id, int value);



    }
}
