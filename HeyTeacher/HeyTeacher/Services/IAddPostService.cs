﻿using HeyTeacherCommon.CommunicationModels;
using System;
using System.Collections.Generic;
using System.Text;
using HeyTeacher.Models;
using System.Threading.Tasks;

namespace HeyTeacher.Services
{
    interface IAddPostService
    {
        Task<List<RequestType>> GetAllPostTypes();

        void AddNewPost(Post post);
    }
}
