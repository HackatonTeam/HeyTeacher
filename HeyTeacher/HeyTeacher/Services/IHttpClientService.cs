﻿using HeyTeacher.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using HeyTeacherCommon.CommunicationModels;

namespace HeyTeacher.Services
{
    public interface IHttpClientService
    {
        User LoggedUser { get; set; }

        Task<T> GetAsync<T>(string path, string token);

        Task<T> PostAsync<T>(string path, object data, string token);

        Task<T> PutAsync<T>(string path, object data, string token);

        Task<string> CallRequest(string path, string method, string requestBody = "");

        Task<List<RequestRank>> GetUserRank(string username);

        string GenerateAuthenticationToken(User user = null);

    }
}
