﻿using HeyTeacherCommon.CommunicationModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace HeyTeacher.Models
{
    public class Post
    {
        public int ID { get; set; }
        public int UserID { get; set; }
        public int DiffLevel { get; set; }
        public string GroupName { get; set; }
        public string Title { get; set; }
        public string PostType { get; set; }
        public string UserName { get; set; }
        public DateTime PublishDate { get; set; }
        public string Content { get; set; }
        public int Rating { get; set; }
        public List<Comment> Comments { get; set; }

        public Post(int _id, string _title, string _postType, int _userID, string _author, string groupName, DateTime _publishDate, string _content, int _rating, int diffLevel, List<Comment> _comments)
        {
            ID = _id;
            Title = _title;
            PostType = _postType;
            UserName = _author;
            UserID = _userID;
            GroupName = groupName;
            PublishDate = _publishDate;
            Content = _content;
            Rating = _rating;
            DiffLevel = diffLevel;
            Comments = _comments;
        }

        public Post(RequestPost requestPost)
        {
            ID = requestPost.Id;
            Title = requestPost.Topic;
            PostType = requestPost.Type;
            UserName = requestPost.Username;
            UserID = requestPost.UserId;
            PublishDate = requestPost.ModifiedDate;
            Content = requestPost.Content;
            Rating = requestPost.Points;
            GroupName = requestPost.GroupName;
            DiffLevel = requestPost.DiffLevel;
            Comments = new List<Comment>();
        }
        
        public string AuthorName
        {
            get
            {
                return UserName;
            }
        }
        public DateTime DateTime
        {
            get
            {
                return PublishDate;
            }
        }

        public static AddPost GetAddPostFromPost(Post post)
        {
            return new AddPost(post.Content, post.GroupName, post.Title, post.PostType, post.DiffLevel);
        }
        public string Description
        {
            get
            {
                int v = this.Content.Length;
                if (v < 100)
                {
                    return this.Content;
                }
                else
                {
                    return this.Content.Substring(0, 97) + "...";
                }
            }
        }
    }
}
