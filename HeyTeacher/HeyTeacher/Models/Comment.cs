﻿using HeyTeacherCommon.CommunicationModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace HeyTeacher.Models
{
    public class Comment
    {
        private int id;
        private User author;
        private DateTime publishDate;
        private string content;
        private int rating;

        public Comment(User author,string content)
        {
            this.id = 0;
            this.author = author;
            this.content = content;
            this.publishDate = DateTime.Now;
            this.rating = 0;
        }

        public Comment(RequestComment requestComment)
        {
            id = requestComment.Id;
            content = requestComment.Content;
            Rating = requestComment.Points;
            PublishDate = requestComment.ModifiedDate;
            author = new User(requestComment.Username, null);
        }

        public int Id
        {
            get => this.id;
        }

        public string UserName
        {
            get => this.author.Username;
        }
        public int Rating
        {
            get => rating;
            set
            {
                this.rating = value;
            }
        }
        public string Content
        {
            get => this.content;
        }
        public DateTime PublishDate
        {
            get => this.publishDate;
            set
            {
                publishDate = value;
            }
        }

        public static AddComment AddCommentFromComment(Comment comment, int PostID)
        {
            return new AddComment(comment.Content, PostID);
        }
    }
}
