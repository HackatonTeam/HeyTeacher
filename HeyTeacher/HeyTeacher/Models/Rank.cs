﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HeyTeacher.Models
{
    class Rank
    {
        public int rank { get; set; }
        public string group { get; set; }

        public Rank(int _rank, string _gr)
        {
            rank = _rank;
            group = _gr;
        }
    }
}
