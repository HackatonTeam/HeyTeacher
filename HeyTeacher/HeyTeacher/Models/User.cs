﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HeyTeacher.Models
{
    public class User
    {
        private string username;
        private string password;

        public string Username { get; set; }
        public string Password { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime Created { get; set; }

        public User(string _username, string _password)
        {
            Username = _username;
            Password = _password;
        }

        public override string ToString()
        {
            return $"Login: {Username}\n" +
                $"FirstName: {FirstName}\n" +
                $"LastName: {LastName}\n" +
                $"Created: {Created.ToString()}";
        }
    }
}
