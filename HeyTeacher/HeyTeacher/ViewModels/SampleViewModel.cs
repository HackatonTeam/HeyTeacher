﻿using HeyTeacher.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace HeyTeacher.ViewModels
{
    class SampleViewModel : ViewModelBase
    {
        private ISampleService _sampleService;
        private string text;

        public SampleViewModel(ISampleService sampleService)
        {
            _sampleService = sampleService;
        }

        public string Text
        {
            get
            {
                return text;
            }
            set
            {
                text = value;
                RaisePropertyChanged(() => Text);
            }
        }

        public ICommand SampleCommand => new Command(TestFunction);

        private void TestFunction() => Text = _sampleService.GetSomeText();
    }
}
