﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;
using System.Windows.Input;
using HeyTeacher.Services;
using HeyTeacher.Models;
using System.Collections.ObjectModel;
using HeyTeacher.Views;
using HeyTeacherCommon.CommunicationModels;
using System.Threading.Tasks;

namespace HeyTeacher.ViewModels
{

    class PostAddPageViewModel : ViewModelBase
    {
        private string title, type, content;
        private ISampleService _sampleService;
        private IAddPostService _addPostService;
        private string _groupName;
        public ObservableCollection<string> type_list { get; set; }

        public PostAddPageViewModel(ISampleService sampleService, IAddPostService addPostService, string groupName)
        {
            _sampleService = sampleService;
            _addPostService = addPostService;
            _groupName = groupName;

            List<RequestType> requestTypes = Task.Run(() => this._addPostService.GetAllPostTypes()).Result;
            type_list = new ObservableCollection<string>();
            foreach (var item in requestTypes)
                type_list.Add(item.Type);
            title = null;
            type = null;
            content = null;
        }

        public string Title
        {
            get
            {
                return title;
            }
            set
            {
                title = value;
                RaisePropertyChanged(() => Title);
            }
        }
        public string Content
        {
            get
            {
                return content;
            }
            set
            {
                content = value;
                RaisePropertyChanged(() => Content);
            }
        }
        public string Type
        {
            get
            {
                return type;
            }
            set
            {
                type = value;
                RaisePropertyChanged(() => Type);
            }
        }
        public ICommand PostFun => new Command(OnPostIt);
        public ICommand AbortFun => new Command(OnAbortIt);

        private void OnPostIt(object obj)
        {
            //check data
            if (null == type || null == content || null == title)
            {
                App.Current.MainPage.DisplayAlert("Alert", "All fields must be fulfilled", "OK");
            }
            else
            {
                _addPostService.AddNewPost(new Post(0, title, type, 0, null, _groupName, new DateTime(), content, 0, 0, null));
                App.Current.MainPage.Navigation.PopModalAsync();
                App.Current.MainPage.DisplayAlert("PostAdded", "Success", "OK");
            }
        }

        private void OnAbortIt(object obj)
        {
            //test danych
            App.Current.MainPage.Navigation.PopModalAsync();
        }

    }
}
