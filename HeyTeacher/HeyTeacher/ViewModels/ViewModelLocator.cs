﻿using Autofac;
using HeyTeacher.Services;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Reflection;
using System.Text;
using Xamarin.Forms;

namespace HeyTeacher.ViewModels
{
    public static class ViewModelLocator
    {
        private static IContainer _container;

        public static readonly BindableProperty AutoWireViewModelProperty =
            BindableProperty.CreateAttached("AutoWireViewModel", typeof(bool), typeof(ViewModelLocator), default(bool), propertyChanged: OnAutoWireViewModelChanged);


        public static bool GetAutoWireViewModel(BindableObject bindable)
        {
            return (bool)bindable.GetValue(ViewModelLocator.AutoWireViewModelProperty);
        }

        public static void SetAutoWireViewModel(BindableObject bindable, bool value)
        {
            bindable.SetValue(ViewModelLocator.AutoWireViewModelProperty, value);
        }


        static ViewModelLocator()
        {
            ContainerBuilder builder = new ContainerBuilder();

            // ViewModels
            builder.RegisterType<MainViewModel>();
            builder.RegisterType<MainMasterViewModel>();
            builder.RegisterType<SampleViewModel>();
            builder.RegisterType <PostViewModel>();
            
            builder.RegisterType<LoginPageViewModel>();
            builder.RegisterType<StartPagePostsViewModel>();
            builder.RegisterType<StartPageMasterViewModel>();
            builder.RegisterType<StartPageViewModel>();
            builder.RegisterType<UserPageViewModel>();
            builder.RegisterType<PostAddPageViewModel>();
            builder.RegisterType<AddUserPageViewModel>();
            builder.RegisterType<GroupViewModel>();

            // Services
            builder.RegisterType<SampleService>().As<ISampleService>().SingleInstance();
            builder.RegisterType<HttpClientService>().As<IHttpClientService>().SingleInstance();
            builder.RegisterType<LoginService>().As<ILoginService>().SingleInstance();
            builder.RegisterType<PostService>().As<IPostService>().SingleInstance();
            builder.RegisterType<UserPageService>().As<IUserPageService>().SingleInstance();
            builder.RegisterType<AddPostService>().As<IAddPostService>().SingleInstance();
            builder.RegisterType<GroupService>().As<IGroupService>().SingleInstance();
            // Others


            _container = builder.Build();
        }

        
        public static T Resolve<T>() where T : class
        {
            return _container.Resolve<T>();
        }
        

        private static void OnAutoWireViewModelChanged(BindableObject bindable, object oldValue, object newValue)
        {
            var view = bindable as Element;
            if (view == null)
            {
                return;
            }

            var viewType = view.GetType();
            var viewName = viewType.FullName.Replace(".Views.", ".ViewModels.");
            var viewAssemblyName = viewType.GetTypeInfo().Assembly.FullName;
            var viewModelName = string.Format(CultureInfo.InvariantCulture, "{0}Model, {1}", viewName, viewAssemblyName);

            var viewModelType = Type.GetType(viewModelName);
            if (viewModelType == null)
            {
                return;
            }
            var viewModel = _container.Resolve(viewModelType);
            view.BindingContext = viewModel;
        }
    }
}
