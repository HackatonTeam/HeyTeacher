﻿using HeyTeacher.Views;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace HeyTeacher.ViewModels
{
    class MainMasterViewModel : ViewModelBase
    {

        public ObservableCollection<MainItem> MenuItems { get; set; }

        public MainMasterViewModel()
        {
            MenuItems = new ObservableCollection<MainItem>(new[]
            {
                    new MainItem { Id = 0, Title = "Page 1" },
                    new MainItem { Id = 4, Title = "Page 2", TargetType = typeof(SampleView)},
                    new MainItem { Id = 12, Title = "Mine", TargetType = typeof(PostView)}
                });
        }
    }
}
