﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;
using System.Windows.Input;
using HeyTeacher.Services;
using HeyTeacher.Models;
using System.Collections.ObjectModel;
using HeyTeacher.Views;
using HeyTeacherCommon.CommunicationModels;
using System.Threading.Tasks;

namespace HeyTeacher.ViewModels
{
    class UserPageViewModel : ViewModelBase
    {
        private ISampleService _sampleService;
        private IHttpClientService _httpClientService;
        private string name;
        private string surname;
        private string username;
        public ObservableCollection<Rank> ranks { get; set; }
        private Image image;

        public UserPageViewModel(ISampleService sampleService, IHttpClientService httpClientService)
        {
            _httpClientService = httpClientService;
            _sampleService = sampleService;
            name = _httpClientService.LoggedUser.FirstName;
            surname = _httpClientService.LoggedUser.LastName;
            username = _httpClientService.LoggedUser.Username;
            Task.Run(() => CreateList(null)).Wait();
        }

        private async void CreateList(object e)
        {
            ranks = new ObservableCollection<Rank>();
            List<RequestRank> tmp = await _httpClientService.GetUserRank(username);
            foreach (var item in tmp)
                ranks.Add(new Rank(item.Rank, item.Group));
            RaisePropertyChanged(() => ranks);
        }

        public UserPageViewModel(ISampleService sampleService, IHttpClientService httpClientService, RequestUser requestUser)
        {
            _sampleService = sampleService;
            _httpClientService = httpClientService;
            name = requestUser.FirstName;
            surname = requestUser.LastName;
            username = requestUser.Username;
            Task.Run(() => CreateList(null)).Wait();
        }

        public Image Picture
        {
            get
            {
                return image;
            }
            set
            {
                image = value;
                RaisePropertyChanged(() => Picture);
            }
        }
        public string Login
        {
            get => username;
        }
        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
                RaisePropertyChanged(() => Name);
            }
        }
        public string Surname
        {
            get
            {
                return surname;
            }
            set
            {
                surname = value;
                RaisePropertyChanged(() => Surname);
            }
        }
        public ICommand AddPost => new Command(OnAddPost);
        public ICommand MyPosts => new Command(OnMyData);

        private void OnAddPost(object obj)
        {
            //add user
            //add user view (from)
            //App.Current.MainPage = new PostAddPageView();
            App.Current.MainPage.Navigation.PushModalAsync(new PostAddPageView("Math"));
        }

        private void OnMyData(object obj)
        {
            //test danych
            App.Current.MainPage.Navigation.PushModalAsync(new StartPagePostsView(new User(username, null)));
        }
    }
}
