﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;
using System.Windows.Input;
using HeyTeacher.Services;
using HeyTeacher.Views;
using HeyTeacher.Models;
using HeyTeacher.Properties;

namespace HeyTeacher.ViewModels
{
    class LoginPageViewModel : ViewModelBase
    {
        private string pass;
        private string login;
        private ISampleService _sampleService;
        private ILoginService _loginService;

        public LoginPageViewModel(ILoginService loginService, ISampleService sampleService)
        {
            _loginService = loginService;
            _sampleService = sampleService;
            Login = "pysqaty";
            Pass = "qwerty";
        }

        public string Pass
        {
            get
            {
                return pass;
            }
            set
            {
                pass = value;
                RaisePropertyChanged(() => Pass);
            }
        }
        public string Login
        {
            get
            {
                return login;
            }
            set
            {
                login = value;
                RaisePropertyChanged(() => Login);
            }
        }
        public ICommand LogFun => new Command(OnLogin);
        public ICommand SignFun => new Command(OnSign);

        private void OnSign(object obj)
        {
            App.Current.MainPage.Navigation.PushModalAsync(new AddUserPageView());
        }

        private async void OnLogin(object obj)
        {
            try
            {
                User user = await _loginService.Login(Login, Pass);
                App.Current.MainPage = new StartPageView();
            }
            catch (Exception e)
            {
                await App.Current.MainPage.DisplayAlert("exception",  e.Message, "OK");
            }
        }
    }
}
