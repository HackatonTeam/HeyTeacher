﻿using HeyTeacher.Views;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Text;
using System.Collections.ObjectModel;

namespace HeyTeacher.ViewModels
{
    class StartPageMasterViewModel : ViewModelBase
    {
        public ObservableCollection<StartPageViewMenuItem> MenuItems { get; set; }

        public StartPageMasterViewModel()
        {
            MenuItems = new ObservableCollection<StartPageViewMenuItem>(new[]
            {
                    new StartPageViewMenuItem { Id = 0, Title = "Top Posts", TargetType = typeof(StartPagePostsView)},
                    new StartPageViewMenuItem { Id = 1, Title = "Account", TargetType = typeof(UserPageView)},
                    new StartPageViewMenuItem { Id = 2, Title = "Groups", TargetType = typeof(GroupView)}
            });
        }
        
    }
}
