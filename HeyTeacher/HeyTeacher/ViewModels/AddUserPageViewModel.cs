﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;
using System.Windows.Input;
using HeyTeacher.Services;
using HeyTeacher.Models;
using System.Collections.ObjectModel;
using HeyTeacher.Views;
using HeyTeacherCommon.CommunicationModels;
using System.Threading.Tasks;

namespace HeyTeacher.ViewModels
{
    class AddUserPageViewModel : ViewModelBase
    {
        private string username, password, firstname, lastname, type;
        private ISampleService _sampleService;
        private IAddPostService _addPostService;
        private ILoginService _loginService;
        public ObservableCollection<string> UserType { get; set; }

        public AddUserPageViewModel(ISampleService sampleService, IAddPostService addPostService, ILoginService loginService)
        {
            _sampleService = sampleService;
            _addPostService = addPostService;
            _loginService = loginService;

            UserType = new ObservableCollection<string>() { "Regular", "Teacher" };
            username = null;
            password = null;
            firstname = null;
            lastname = null;
            type = null;
        }

        public string Username
        {
            get
            {
                return username;
            }
            set
            {
                username = value;
                RaisePropertyChanged(() => Username);
            }
        }
        public string Password
        {
            get
            {
                return password;
            }
            set
            {
                password = value;
                RaisePropertyChanged(() => Password);
            }
        }
        public string Type
        {
            get
            {
                return type;
            }
            set
            {
                type = value;
                RaisePropertyChanged(() => Type);
            }
        }
        public string FirstName
        {
            get
            {
                return firstname;
            }
            set
            {
                firstname = value;
                RaisePropertyChanged(() => FirstName);
            }
        }
        public string LastName
        {
            get
            {
                return lastname;
            }
            set
            {
                lastname = value;
                RaisePropertyChanged(() => LastName);
            }
        }
        public ICommand AddUser => new Command(OnAddUser);
        public ICommand AbortFun => new Command(OnAbortIt);

        private async void OnAddUser(object obj)
        {
            //check data
            if (null == type || null == lastname || null == firstname || null == password || null == username)
            {
                await App.Current.MainPage.DisplayAlert("Alert", "All fields must be fulfilled", "OK");
            }
            else
            {
                try
                {
                    User user = await _loginService.AddNewUser(username,password,firstname,lastname,type);
                    await App.Current.MainPage.DisplayAlert("Success", user.ToString(), "OK");
                    App.Current.MainPage = new StartPageView();
                }
                catch (Exception e)
                {
                    await App.Current.MainPage.DisplayAlert("exception", e.Message, "OK");
                }
            }
        }

        private void OnAbortIt(object obj)
        {
            //test danych
            App.Current.MainPage.Navigation.PopModalAsync();
        }
    }
}
