﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using System.Windows.Input;
using HeyTeacher.Models;
using HeyTeacher.Services;
using HeyTeacher.Views;
using Xamarin.Forms;
using HeyTeacherCommon.CommunicationModels;
using HeyTeacherCommon.CommunicationModels.CommunicationModels;

namespace HeyTeacher.ViewModels
{
    class StartPagePostsViewModel : ViewModelBase
    {
        ObservableCollection<Post> ListPost;
        private IPostService _postService;
        private IAddPostService _addPostService;
        private ISampleService _sampleService;
        private IHttpClientService _httpClientService;
        private IGroupService _groupService;
        private bool join;
        private bool inGroup;
        private User _user;
        private string _groupName;

        public StartPagePostsViewModel(IPostService postService, ISampleService sampleService, IHttpClientService httpClientService, User user = null)
        {
            _httpClientService = httpClientService;
            _postService = postService;
            _sampleService = sampleService;
            _user = user;
            _groupName = null;
            letMeJoin = false;
            isInGroup = false;
            ListPost = new ObservableCollection<Post>();
            Task.Run(() => Refresh(null)).Wait();

        }

        public StartPagePostsViewModel(IPostService postService, ISampleService sampleService, IHttpClientService httpClientService, IGroupService groupService, string groupName, bool isIn, User user)
        {
            _httpClientService = httpClientService;
            _postService = postService;
            _sampleService = sampleService;
            _groupService = groupService;
            _groupName = groupName;
            _user = user;
            if (isIn)
            {
                isInGroup = true;
                letMeJoin = false;
            }
            else
            {
                letMeJoin = true;
                isInGroup = false;
            }

            ListPost = new ObservableCollection<Post>();
            Task.Run(() => Refresh(null)).Wait();
        }

        public bool letMeJoin
        {
            get
            {
                return join;
            }
            set
            {
                join = value;
                RaisePropertyChanged(() => letMeJoin);
            }
        }

        public bool isInGroup
        {
            get
            {
                return inGroup;
            }
            set
            {
                inGroup = value;
                RaisePropertyChanged(() => isInGroup);
            }
        }

        public ICommand RefreshClick => new Command(Refresh);
        public ICommand MoreClick => new Command(More);
        public ICommand JoinToGroup => new Command(Join);
        public ICommand AddPost => new Command(CreatePost);

        private void CreatePost(object obj)
        {
            App.Current.MainPage.Navigation.PushModalAsync(new PostAddPageView(_groupName));
        }

        private void Join(object obj)
        {
            _groupService.JoinGroup(_groupName, _user.Username);
            isInGroup = true;
            letMeJoin = false;
            App.Current.MainPage.DisplayAlert("Group join", "You have joined group", "OK");
        }

        private void More(object obj)
        {
            App.Current.MainPage.Navigation.PushModalAsync(new PostView(obj as Post));
        }

        private async void Refresh(object obj)
        {
            List<Post> posts = new List<Post>();
            if (_groupName != null)
            {
                posts = await _groupService.GetPostsByGroupName(_groupName);
            }
            else
            {
                if (_user != null)
                {
                    posts = await _postService.GetAllUserPosts(_user);
                }
                else
                    posts = await _postService.GetAllPosts();
            }
            ListPost.Clear();
            foreach (Post p in posts)
                ListPost.Add(p);
        }

        public ObservableCollection<Post> Posts
        {
            get
            {
                return ListPost;
            }
            set
            {
                ListPost = value;
                RaisePropertyChanged(() => Posts);
            }
        }

       
    }
}
