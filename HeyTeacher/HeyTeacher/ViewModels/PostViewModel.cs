﻿using HeyTeacher.Models;
using HeyTeacher.Services;
using HeyTeacher.Views;
using HeyTeacherCommon.CommunicationModels;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HeyTeacher.ViewModels
{
    class PostViewModel : ViewModelBase
    {
        private IUserPageService _userPageService;
        private IHttpClientService _httpClientService;
        private IPostService _postService;
        private Post post;
        private string UserImage = "user.png";
        public string Constent
        {
            get
            {
                return post.Content;
            }
        }
        public PostViewModel(IUserPageService userPageService, IHttpClientService httpClientService, IPostService postService)
        {
            _userPageService = userPageService;
            _httpClientService = httpClientService;
            _postService = postService;
        }

        public PostViewModel(IUserPageService userPageService, IHttpClientService httpClientService, IPostService postService, Post _post)
        {
            _userPageService = userPageService;
            _httpClientService = httpClientService;
            _postService = postService;
            post = _post;
            List<Comment> comments = Task.Run(() => _postService.GetAllCommentsToPost(_post)).Result;
            post.Comments = comments;
        }

        public string Title
        {
            get => post.Title;
        }
        public string UserName
        {
            get => post.AuthorName;
        }
        public DateTime Date
        {
            get => post.PublishDate;
        }
        public string Content
        {
            get => post.Content;
        }
        public int Rating
        {
            get => post.Rating;
            set {
                post.Rating = value;
            }
        }
        public HtmlWebViewSource ContentHTML
        {
            get
            {
                var source = new HtmlWebViewSource();
                var text = "<html>" +
                        "<body  style=\"text-align: justify;\">" +
                        String.Format("<p>{0}</p>", post.Content) +
                        "</body>" +
                        "</html>";
                source.Html = text;
                return source;
            }
        }
        public ObservableCollection<Comment> Comments
        {
            get
            {
                return new ObservableCollection<Comment>(post.Comments);
            }
        }
        public string CommentsNumber
        {
            get
            {
                RaisePropertyChanged(() => CommentsNumber);
                return post.Comments.Count.ToString();
            }
        }
        public ICommand IncreaseRatingFun => new Command(IncreaseRating);
        public ICommand DecreaseRatingFun => new Command(DecreaseRating);
        public ICommand IncreaseCommentRatingFun => new Command(IncreaseCommentRating);
        public ICommand DecreaseCommentRatingFun => new Command(DecreaseCommentRating);
        public ICommand UserClick => new Command(ShowUser);

        private async void ShowUser(object obj)
        {
            RequestUser requestUser = await _userPageService.GetUserByLogin(post.UserName);
            if(post.UserName.Equals(_httpClientService.LoggedUser.Username))
                await App.Current.MainPage.Navigation.PushModalAsync(new UserPageView(requestUser));
            else
                await App.Current.MainPage.Navigation.PushModalAsync(new StartPagePostsView(new User(requestUser.Username, null)));
        }

        private void IncreaseRating()
        {
            this.post.Rating = this.post.Rating+1;
            RaisePropertyChanged(() => Rating);
        }
        public void DecreaseRating()
        {
            this.post.Rating--;
            RaisePropertyChanged(() => Rating);
        }
        private void IncreaseCommentRating(object comment)
        {
            Comment com = comment as Comment;
            com.Rating++;
            RaisePropertyChanged(() => Comments);
        }
        private void DecreaseCommentRating(object comment)
        {
            Comment com = comment as Comment;
            com.Rating--;
            int p = Comments.IndexOf(com);
            RaisePropertyChanged(() => Comments);
        }
        private string newCommentText;
        public string NewComment
        { get
            {
                return newCommentText;
            }
            set
            {
                newCommentText = value;
                RaisePropertyChanged(() => NewComment);
            }
        }
        public ICommand AddComment => new Command(AddCommentFun);
        private async void AddCommentFun()
        {
            Comment comment = new Comment(_httpClientService.LoggedUser, newCommentText);
            try
            {
                await _postService.AddNewComment(newCommentText, post.ID);
                this.post.Comments.Add(comment);
                this.newCommentText = "";
                RaisePropertyChanged(() => Comments);
                RaisePropertyChanged(() => NewComment);
            }
            catch (Exception e)
            {
                await App.Current.MainPage.DisplayAlert("exception", e.Message, "OK");
            }

        }
    }
}
