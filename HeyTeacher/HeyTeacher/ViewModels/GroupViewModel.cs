﻿using HeyTeacher.Services;
using HeyTeacher.Views;
using HeyTeacherCommon.CommunicationModels.CommunicationModels;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace HeyTeacher.ViewModels
{
    class GroupViewModel : ViewModelBase
    {
        private IGroupService _groupService;
        private IHttpClientService _httpClientService;
        public ObservableCollection<string> UserGroups { get; set; }

        public ObservableCollection<string> SearchedGroups { get; set; }

        private List<string> OtherGroups { get; set; }

        private string searchText;

        public string SearchText
        {
            get
            {
                return searchText;
            }
            set
            {
                searchText = value;
                RaisePropertyChanged(() => SearchText);
            }
        }
    
        public ICommand Search => new Command(SearchFunction);

        public ICommand MoveToPosts => new Command(MoveToPostsFun);


        private void SearchFunction()
        {
            SearchedGroups.Clear();
            string searchLowerCase = SearchText.ToLower();
            foreach (string groupName in OtherGroups)
            {
                if (!UserGroups.Contains(groupName))
                {
                    string groupNameLower = groupName.ToLower();
                    if (groupNameLower.StartsWith(searchLowerCase))
                    {
                        SearchedGroups.Add(groupName);
                    }
                }
            }
        }

        private void MoveToPostsFun(object groupName)
        {
            bool isIn = false;
            if (UserGroups.Contains(groupName as string))
                isIn = true;

            var page = new StartPagePostsView(groupName as string, isIn, new Models.User(_httpClientService.LoggedUser.Username, null));
                ((MasterDetailPage)App.Current.MainPage).Detail = new NavigationPage(page);
                ((MasterDetailPage)App.Current.MainPage).IsPresented = false;
            
        }

        public GroupViewModel(IGroupService groupService, IHttpClientService httpClientService)
        {
            _groupService = groupService;
            _httpClientService = httpClientService;
            List<RequestGroup> groupList = new List<RequestGroup>();
            List<RequestGroup> otherList = new List<RequestGroup>();

            otherList = Task.Run(() => _groupService.GetUserGroups(_httpClientService.LoggedUser.Username)).Result;
            groupList = Task.Run(() => _groupService.GetOtherGroups(_httpClientService.LoggedUser.Username)).Result;

            UserGroups = new ObservableCollection<string>();
            SearchedGroups = new ObservableCollection<string>();
            OtherGroups = new List<string>();
            foreach (RequestGroup rg in groupList)
                UserGroups.Add(rg.GroupName);

            foreach (RequestGroup rg in otherList)
                OtherGroups.Add(rg.GroupName);

            
            foreach (string groupName in OtherGroups)
            {
                if (!UserGroups.Contains(groupName))
                {
                    SearchedGroups.Add(groupName);
                }
            }

            searchText = "";
        }

    }
}
