﻿using HeyTeacher.Views;
using System;
using System.Collections.Generic;
using System.Text;

namespace HeyTeacher.ViewModels
{
    class MainItem
    {
        public MainItem()
        {
            TargetType = typeof(MainDetailView);
        }
        public int Id { get; set; }
        public string Title { get; set; }

        public Type TargetType { get; set; }
    }
}
