﻿using HeyTeacher.Services;
using HeyTeacher.ViewModels;
using HeyTeacherCommon.CommunicationModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HeyTeacher.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class UserPageView : ContentPage
	{
		public UserPageView()
		{
			InitializeComponent();
            ISampleService sampleService = ViewModelLocator.Resolve<ISampleService>();
            IHttpClientService httpClientService = ViewModelLocator.Resolve<IHttpClientService>();
            BindingContext = new UserPageViewModel(sampleService, httpClientService);
        }

        public UserPageView(RequestUser user)
        {
            InitializeComponent();
            ISampleService sampleService = ViewModelLocator.Resolve<ISampleService>();
            IHttpClientService httpClientService = ViewModelLocator.Resolve<IHttpClientService>();
            BindingContext = new UserPageViewModel(sampleService, httpClientService, user);
        }
	}
}