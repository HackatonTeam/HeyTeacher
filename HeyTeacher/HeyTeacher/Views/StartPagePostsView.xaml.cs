﻿using HeyTeacher.Models;
using HeyTeacher.Services;
using HeyTeacher.ViewModels;
using HeyTeacherCommon.CommunicationModels.CommunicationModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HeyTeacher.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class StartPagePostsView : ContentPage
    {
        public StartPagePostsView()
        {
            InitializeComponent();
            BindingContext = new StartPagePostsViewModel(ViewModelLocator.Resolve<IPostService>(), ViewModelLocator.Resolve<ISampleService>(), ViewModelLocator.Resolve<IHttpClientService>());
        }

        public StartPagePostsView(User user)
        {
            InitializeComponent();
            BindingContext = new StartPagePostsViewModel(ViewModelLocator.Resolve<IPostService>(), ViewModelLocator.Resolve<ISampleService>(), ViewModelLocator.Resolve<IHttpClientService>(), user);
        }

        public StartPagePostsView(string groupName, bool isIn, User user)
        {
            InitializeComponent();
            BindingContext = new StartPagePostsViewModel(ViewModelLocator.Resolve<IPostService>(), ViewModelLocator.Resolve<ISampleService>(), ViewModelLocator.Resolve<IHttpClientService>(), ViewModelLocator.Resolve<IGroupService>(), groupName, isIn, user);
        }
    }
}