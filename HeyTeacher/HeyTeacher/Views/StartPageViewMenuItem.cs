﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HeyTeacher.Views
{

    public class StartPageViewMenuItem
    {
        public StartPageViewMenuItem()
        {
            TargetType = typeof(StartPagePostsView);
        }
        public int Id { get; set; }
        public string Title { get; set; }

        public Type TargetType { get; set; }
    }
}