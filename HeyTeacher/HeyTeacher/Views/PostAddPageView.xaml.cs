﻿using HeyTeacher.Services;
using HeyTeacher.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HeyTeacher.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class PostAddPageView : ContentPage
	{
		public PostAddPageView (string groupName)
		{
			InitializeComponent ();
            BindingContext = new PostAddPageViewModel(ViewModelLocator.Resolve<ISampleService>(), ViewModelLocator.Resolve<IAddPostService>(), groupName);
		}
	}
}