﻿using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HeyTeacher.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MainMasterView : ContentPage
    {
        public ListView ListView;

        public MainMasterView()
        {
            InitializeComponent();
            ListView = MenuItemsListView;
        }
        
    }
}