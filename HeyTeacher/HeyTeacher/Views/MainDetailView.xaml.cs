﻿using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HeyTeacher.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MainDetailView : ContentPage
    {
        public MainDetailView()
        {
            InitializeComponent();
        }
    }
}