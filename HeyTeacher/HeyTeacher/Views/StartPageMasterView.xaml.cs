﻿using HeyTeacher.ViewModels;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HeyTeacher.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class StartPageMasterView : ContentPage
    {
        public ListView ListView;

        public StartPageMasterView()
        {
            InitializeComponent();
            ListView = MenuItemsListView;
        }
    }
}