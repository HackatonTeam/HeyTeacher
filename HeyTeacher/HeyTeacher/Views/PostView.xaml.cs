﻿using HeyTeacher.Models;
using HeyTeacher.Services;
using HeyTeacher.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HeyTeacher.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class PostView : ContentPage
	{
        public PostView(Post post)
        {
            InitializeComponent();
            IUserPageService userPageService = ViewModelLocator.Resolve<IUserPageService>();
            IHttpClientService httpClientService = ViewModelLocator.Resolve<IHttpClientService>();
            IPostService postService = ViewModelLocator.Resolve<IPostService>();
            BindingContext = new PostViewModel(userPageService, httpClientService, postService, post);
        }
	}
}