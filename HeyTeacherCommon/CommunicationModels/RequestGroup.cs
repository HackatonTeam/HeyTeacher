﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HeyTeacherCommon.CommunicationModels.CommunicationModels
{
    public class RequestGroup
    {
        public int GroupId { get; protected set; }
        public string GroupName { get; protected set; }

        public RequestGroup(int groupId, string groupname)
        {
            GroupId = groupId;
            GroupName = groupname;
        }
    }
}
