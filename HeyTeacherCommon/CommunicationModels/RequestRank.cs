﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HeyTeacherCommon.CommunicationModels
{
    public class RequestRank
    {
        public string Group { get; protected set; }
        public int Rank { get; protected set; }

        public RequestRank(string group, int rank)
        {
            Group = group;
            Rank = rank;
        }
    }
}
