﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HeyTeacherCommon.CommunicationModels
{
    public class RatePost
    {
        public int Value { get; protected set; }

        public RatePost(int value)
        {
            Value = value;
        }
    }
}
