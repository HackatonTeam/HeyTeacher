﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HeyTeacherCommon.CommunicationModels
{
    public class RequestPost
    {
        public int Id { get; protected set; }
        public string Content { get; protected set; }
        public DateTime ModifiedDate { get; protected set; }
        public int UserId { get; protected set; }
        public string Username { get; protected set; }
        public int GroupId { get; protected set; }
        public string GroupName { get; protected set; }
        public int Points { get; protected set; }
        public string Type { get; protected set; }
        public string Topic { get; protected set; }
        public int Color { get; protected set; }
        public int UserRating { get; protected set; }
        public int DiffLevel { get; protected set; }

        public RequestPost(int id, string content, DateTime modifiedDate, int userid, string username, int groupid, string groupname, int points, string type, string topic, int color, int userRating, int diffLevel=0)
        {
            Id = id;
            Content = content;
            ModifiedDate = modifiedDate;
            UserId = userid;
            Username = username;
            GroupId = groupid;
            GroupName = groupname;
            Points = points;
            Type = type;
            Topic = topic;
            Color = color;
            UserRating = userRating;
            DiffLevel = diffLevel;
        }
    }
}
