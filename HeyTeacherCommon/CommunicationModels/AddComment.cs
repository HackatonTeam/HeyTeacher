﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HeyTeacherCommon.CommunicationModels
{
    public class AddComment
    {
        public string Content { get; protected set; }
        public int PostId { get; protected set; }

        public AddComment(string content, int postid)
        {
            Content = content;
            PostId = postid;
        }
    }
}
