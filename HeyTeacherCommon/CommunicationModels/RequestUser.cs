﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HeyTeacherCommon.CommunicationModels
{
    public class RequestUser
    {
        public string Username { get; protected set; }
        public string FirstName { get; protected set; }
        public string LastName { get; protected set; }
        public DateTime Created { get; protected set; }

        public RequestUser(string username, string firstname, string lastname, DateTime created)
        {
            Username = username;
            FirstName = firstname;
            LastName = lastname;
            Created = created;
        }
    }
}
