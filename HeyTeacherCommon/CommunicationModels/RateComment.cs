﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HeyTeacherCommon.CommunicationModels
{
    public class RateComment
    {
        public int Value { get; protected set; }

        public RateComment(int value)
        {
            Value = value;
        }
    }
}
