﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HeyTeacherCommon.CommunicationModels
{
    public class AddPost
    {
        public string Content { get; protected set; }
        public string GroupName { get; protected set; }
        public string Title { get; protected set; }
        public string Type { get; protected set; }
        public int DiffLevel { get; protected set; }

        public AddPost(string content, string groupname, string title, string type, int diffLevel)
        {
            Content = content;
            GroupName = groupname;
            Title = title;
            Type = type;
            DiffLevel = diffLevel;
        }
    }
}
