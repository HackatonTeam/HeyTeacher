﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HeyTeacherCommon.CommunicationModels
{
    
    public class RequestType
    {
        public string Type { get; protected set; }
        public int Color { get; protected set; }

        public RequestType(string type, int color)
        {
            Type = type;
            Color = color;
        }
    }
}
