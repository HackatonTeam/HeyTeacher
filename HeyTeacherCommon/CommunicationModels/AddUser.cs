﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HeyTeacherCommon.CommunicationModels
{
    public class AddUser
    {
        public string Login { get; protected set; }
        public string Password { get; protected set; }
        public string FirstName { get; protected set; }
        public string LastName { get; protected set; }
        public string Type { get; protected set; }

        public AddUser(string login, string password, string firstname, string lastname, string type)
        {
            Login = login;
            Password = password;
            FirstName = firstname;
            LastName = lastname;
            Type = type;
        }
    }
}
