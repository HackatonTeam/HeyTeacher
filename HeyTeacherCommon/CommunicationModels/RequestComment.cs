﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HeyTeacherCommon.CommunicationModels
{
    public class RequestComment
    {
        public int Id { get; protected set; }
        public string Content { get; protected set; }
        public DateTime ModifiedDate { get; protected set; }
        public int UserId { get; protected set; }
        public string Username { get; protected set; }
        public int Points { get; protected set; }
        public string Type { get; protected set; }
        public int UserRating { get; protected set; }

        public RequestComment(int id, string content, DateTime modifiedDate, int userid, string username, int points, int userRating)
        {
            Id = id;
            Content = content;
            ModifiedDate = modifiedDate;
            UserId = userid;
            Username = username;
            Points = points;
            UserRating = userRating;
        }
    }
}
